# \\/\\/under Task :man_technologist:
 
**Requirements:**
 
	- PHP 7 (7.2 used)
	- Database: MySQL/MariaDB (mysqli extension used)
	- Composer ($ composer install)
	- Web public directory: ./www/
	- Httpd server index file (FrontController): index.php
	- Edit config file: ./config/main.ini
	- Database Scheme: database.sql 

**1. Describe possible performance optimizations for your Code.**

	- (code) Profiling all the code
	- (code) Php use of OPcache
	- (code) Php views cache
	- (code) Put db connection outside bootstrap.php to just be called when needed
	- (code) Separation of the content by ajax requests VS the one load of all content to the client
	- (code) 'Minification' of html,css,js
	- (code) Browser cache
	- (infrastructure) Create a php extension if really needed
	- (infrastructure) Save pre registrations in a memory database nosql (eg: redis, memcache, ...)
	- (infrastructure) Analyze database setup (indexing, storage mode, columns types, etc)
	- (infrastructure) Compress (gzip) content at http server
	- (infrastructure) Tunning backend (http, database, ...)
	- (infrastructure) Cache static files or put them at a CDN

**2. Which things could be done better, than you've done it?**

	- Use of a mature framework would be advantageous for a mid/long term project (documentation, components, template engine, ...)
	- Use of a difference database driver, mysqli seems to be old already
	- Testing: Unit, Functional, ...
	- Profiling all the code
	- Create code using known principles/standards
	
**It will be a big plus if you structure your code using the MVP/MVI/MVVM pattern...(and let us know why)**

	- I used a MVP|MVC(?) pattern because of the separation of concerns that bring some sort of modularity
	