<?php
namespace Controllers;

use Library\Controller;

/**
 * Error manager
 */
class Error extends Controller
{

    /**
     * Error Factory
     *
     * @param string|int $error
     */
    public function indexAction($error = false)
    {
        switch ($error) {
            case '404':
                $this->error404();
                break;

            // @todo implement other errors

            default:
                'error';
                break;
        }
    }

    /**
     * Error - not found - 404
     */
    private function error404()
    {
        header("HTTP/1.0 404 Not Found");
        die('Error 404 - Not Found');
    }
}