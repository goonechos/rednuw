<?php
namespace Controllers;

use Library\Controller;
use Library\View;
use Library\Config;

/**
 * Default Controller
 *
 * @todo: Check how to proper secure the data at cookie. There
 * should be better solutions for this kind of stuff outhere !!!
 */
class Index extends Controller
{

    /**
     * Library\View Obj
     */
    public $view;

    /**
     * Library\Config Obj
     */
    public $config;

    /**
     * Exec before any action
     */
    public function init($action = '')
    {
        // view access
        $this->view = new View();
        $this->config = (new Config())::get();

        // default vars
        $this->view->firstname = '';
        $this->view->lastname = '';
        $this->view->telephone = '';
        $this->view->street = '';
        $this->view->housenumber = '';
        $this->view->zipcode = '';
        $this->view->city = '';
        $this->view->accountowner = '';
        $this->view->iban = '';
    }

    /**
     * Default action
     */
    public function indexAction()
    {
        $this->view->set('homepage');
    }

    /**
     * User registration (form + database registration)
     */
    public function registerAction()
    {
        // create new session
        if (! isset($_COOKIE['sessionRegistration'])) {

            $cookie_safety = $this->getCookieSafety();

            $modelSession = new \Models\Sessions();
            $modelSession->cookie_value = $cookie_safety['hash'];
            $modelSession->cookie_extra_salt = $cookie_safety['private_salt'];
            $modelSession->step = 1;
            $modelSession->save();

            if (is_numeric($modelSession->id)) {
                setcookie('sessionRegistration', $cookie_safety['hash'], strtotime('+366 days'));

                $this->view->step = '1';

                return;
            }
        }

        // verify cookie
        $checkSession = $this->checkSession();

        if ($checkSession) {

            $this->view->step = $checkSession->step;

            $form_fields = json_decode($checkSession->form_fields, true);
            if ($form_fields)
                array_walk($form_fields, function ($value, $key) {
                    $this->view->{$key} = $value;
                });
        } else {
            // reset cookie if cookie value at db disappear
            setcookie('sessionRegistration', '');
            header('Location: /?action=register');
            return;
        }

        // User registration on DB
        if (isset($_POST) && $_POST) {

            $modelRegistrations = new \Models\Registrations();
            $modelRegistrations->startTransaction();
            $modelRegistrations->form_fields = $checkSession->form_fields;
            $modelRegistrations->save();

            if (is_numeric($modelRegistrations->id)) {

                // verify config api endpoint
                if (! $this->config->api->endpoint)
                    throw new \Exception('You need to set at config/main.ini the api endpoint!');
                
                $fields = json_decode($checkSession->form_fields, true);

                // payment api request
                $postParameters = [
                    'customerId' => $modelRegistrations->id,
                    'iban' => $fields['iban'],
                    'owner' => $fields['accountowner']
                ];
                
                $postRequest = (new \GuzzleHttp\Client([
                    'verify' => false,
                    'http_errors' => false,
                    'debug' => false
                ]))->post($this->config->api->endpoint, [
                    'debug' => false,
                    'body' => json_encode($postParameters)
                ]);

                $postResponseCode = $postRequest->getStatusCode();
                $postResponseContent = $postRequest->getBody()->getContents();

                if ($postResponseCode == 200) {

                    // save payment id
                    $arrPostResponsetContent = json_decode($postResponseContent, true);
                    $modelRegistrations->payment_data_id = $arrPostResponsetContent['paymentDataId'];
                    $modelRegistrations->save();

                    // delete pre registration
                    $checkSession->delete();

                    // mysqli commit
                    $modelRegistrations->commit();

                    $this->view->payment_data_id = $modelRegistrations->payment_data_id;
                    $this->view->set('successful');

                    // clean cookie
                    setcookie('sessionRegistration', '');
                    
                } else {

                    // show api error
                    $modelRegistrations->rollback();
                    $this->view->error_code = $postResponseCode;
                    $this->view->error_message = $postResponseContent;
                }
            }
        }
    }

    /**
     * Save input and step state
     */
    public function logAction()
    {
        if (isset($_POST) && $_POST) {
            $checkSession = $this->checkSession();
            if ($checkSession) {

                // save fields
                if (isset($_POST['field']) && $_POST['field'] && isset($_POST['value'])) {
                    $form_fields = json_decode($checkSession->form_fields, true);
                    $form_fields[$_POST['field']] = $_POST['value'];
                    $checkSession->form_fields = json_encode($form_fields);
                }

                // save step
                if (isset($_POST['step']) && is_numeric($_POST['step']) && $_POST['step'] > 0 && $_POST['step'] < 4) {
                    $checkSession->step = $_POST['step'];
                }

                // persist at db
                $checkSession->save();

                // update cookie expiration date
                setcookie('sessionRegistration', $checkSession->cookie_value, strtotime('+366 days'));
            }
        }

        $this->view->disable();
    }

    /**
     * Verify session by cookie
     *
     * @return object|boolean
     */
    private function checkSession()
    {
        if (isset($_COOKIE['sessionRegistration']) && $_COOKIE['sessionRegistration']) {

            $check_cookie = (new \Models\Sessions())::retrieveByField('cookie_value', $_COOKIE['sessionRegistration'], \SimpleOrm::FETCH_ONE);

            $hash = sha1(sha1($_SERVER['HTTP_USER_AGENT'] . $check_cookie->cookie_extra_salt . $this->config->app->salt));

            if ($check_cookie->cookie_value == $hash)
                return $check_cookie;
        }

        return false;
    }

    /**
     * Get $_COOKIE[sessionRegistration] value hash and salt
     *
     * @return array
     */
    private function getCookieSafety()
    {
        $private_salt = sha1(bin2hex(random_bytes(55)));

        $hash = sha1(sha1($_SERVER['HTTP_USER_AGENT'] . $private_salt . $this->config->app->salt));

        $cookie_safety = [
            'hash' => $hash,
            'private_salt' => $private_salt
        ];

        return $cookie_safety;
    }
}