<!DOCTYPE html>
<html>

    <head>
        <title>\/\/under Task - 2018</title>
        <script src="./assets/js/jquery.min.js"></script>
        <script src="./assets/js/main.js"></script>
        <link href="./assets/css/main.css" type="text/css" rel="stylesheet" />
        <link href="./assets/css/bootstrap.min.css" type="text/css" rel="stylesheet" >
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    </head>
    
    <body>
    
    	<h1><a href="./?action=index">Home</a> / Register</h1>
    	
    	<div id="steps">
        	<form method="post" action="./?action=register">
        		<div id="step1" class="step">
        			<h2>Step 1/4 - personal information</h2>
        			
        			<span>First Name<br> <input type="text" name="firstname" value="<?=$firstname?>"></span> <br>
        			<span>Last Name<br> <input type="text" name="lastname" value="<?=$lastname?>"></span> <br>
        			<span>Telephone<br> <input type="text" name="telephone" value="<?=$telephone?>"></span> <br>
        			
        			<button class="jump btn btn-secundary" data-from="1" data-to="2">Next</button>
        			<span class="saving"><i class="far fa-save"></i></span>
        		</div>
        		
        		<div id="step2" class="step">
        			<h2>Step 2/4 - address information</h2>
        			
        			<span>Street<br> <input type="text" name="street" value="<?=$street?>"></span> <br>
        			<span>house number<br> <input type="number" name="housenumber" value="<?=$housenumber?>"></span> <br>
        			<span>zip code<br> <input type="text" name="zipcode" value="<?=$zipcode?>"></span> <br>
        			<span>city<br> <input type="text" name="city" value="<?=$city?>"></span> <br>
        			
        			<button type="button"  class="jump btn btn-secundary" data-from="2" data-to="1">Back</button>
        			<button class="jump btn btn-secundary" data-from="2" data-to="3">Next</button>
        			<span class="saving"><i class="far fa-save"></i></span>
        		</div>
        		
        		<div id="step3" class="step">
        			<h2>Step 3/4 - payment information</h2>
        			
        			<span>Account owner<br> <input type="text" name="accountowner" value="<?=$accountowner?>"></span> <br>
        			<span>IBAN<br> <input type="text" name="iban" value="<?=$iban?>"></span> <br>
        			
        			<button type="button" class="jump btn btn-secundary" data-from="3" data-to="2">Back</button>
        			<button class="register btn btn-primary" data-step="Register">Register</button>
        			<span class="saving"><i class="far fa-save"></i></span>
        		</div>
        	</form>
        	
        	<?php if(isset($error_message) && $error_message): ?>
        		<div class="error">
        			Error code: <?=$error_code?> <br>
        			Error raw content: <?= $error_message ?>
        		</div>
        	<?php endif; ?>
        	
    	</div>
    	
    	<script>
    		main.steps.step = '<?=$step?>';
    	</script>
    
    </body>

</html>