<?php

// loader
require_once __DIR__ . '/library/loader.php';

// config
$config = (new Library\Config())::get();

// constants
define('CORE_ENVIRONMENT', $config->app->stage);
define('CORE_ERROR_REPORT', $config->app->stage == 'development' ? E_ALL : 0);

// server settings
require_once __DIR__ . '/library/settings.php';

try {

    // db
    $conn = new \mysqli($config->db->host, $config->db->username, $config->db->password);

    if ($conn->connect_error)
        throw new \Exception(sprintf('Unable to connect to the database. %s', $conn->connect_error));

    SimpleOrm::useConnection($conn, $config->db->database);

    // dispatcher
    $action = (isset($_GET['action']) && $_GET['action']) ? $_GET['action'] : 'index';

    $app = new Controllers\Index();

    if (method_exists($app, $action . 'Action')) {

        $app->init($action);
        $app->{$action . 'Action'}();
        $app->endit($action);
        
    } else
        (new Controllers\Error())->indexAction('404');
    
} catch (Exception $e) {

    // @todo manage exceptions + errors

    echo '<pre>';
    var_dump($e->getMessage());
    echo '<hr>';
    var_dump($e->getTrace());
    echo '</pre>';
}