<?php
namespace Models;

use Library\Model;

/**
 * Table to save pre registrations
 */
class Sessions extends Model
{

    protected static $table = 'sessions';
}