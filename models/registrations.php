<?php
namespace Models;

use Library\Model;

/**
 * Table to save users registrations
 */
class Registrations extends Model
{

    protected static $table = 'users_registrations';
}