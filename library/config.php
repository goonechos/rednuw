<?php
namespace Library;

/**
 * Config class
 */
class Config
{

    public static $config;

    /**
     * __construct
     */
    public function __construct()
    {
        $config = parse_ini_file("../config/main.ini", TRUE);
        self::$config = json_decode(json_encode($config));
    }

    /**
     * Return config
     *
     * @return object
     */
    public static function get()
    {
        return self::$config;
    }
}