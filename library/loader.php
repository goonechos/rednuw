<?php

require_once '../vendor/autoload.php';

$loader = new Nette\Loaders\RobotLoader;

// Add directories for RobotLoader to index
$loader->addDirectory(__DIR__ . '/../library');
$loader->addDirectory(__DIR__ . '/../vendor');
$loader->addDirectory(__DIR__ . '/../controllers');
$loader->addDirectory(__DIR__ . '/../models');

// And set caching to the 'temp' directory
$loader->setTempDirectory(__DIR__ . '/../temp');
$loader->register(); // Run the RobotLoader