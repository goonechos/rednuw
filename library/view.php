<?php
namespace Library;

/**
 * View Class
 */
class View
{

    /**
     * Exchange variables to the views
     */
    private static $vars = [];

    /**
     * To verify if the view is already setted up
     *
     * @var bool
     */
    public static $setted = false;

    /**
     * To verify the disableless of the view for an action
     *
     * @var bool
     */
    public static $disabled = false;

    /**
     * Set view file
     *
     * @param string $view_file
     */
    public static function set($view_file)
    {
        $file = ".." . DIRECTORY_SEPARATOR . 'views' . DIRECTORY_SEPARATOR . $view_file . ".php";

        if (file_exists($file)) {

            extract(self::$vars);
            require_once ($file); // @todo http://php.net/manual/en/ref.outcontrol.php
            self::$setted = true;
            
        } else
            throw new \Exception('View file ' . $view_file . '.php doesn\'t exist!');
    }

    /**
     * Disable view
     */
    public static function disable()
    {
        self::$disabled = true;
    }

    /**
     * Set dynamic var
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value)
    {
        self::$vars[$name] = $value;
    }

    /**
     * Get dynamic var
     *
     * @param string $name
     * @return mixed
     */
    public function __get($name)
    {
        return self::$vars[$name];
    }
}