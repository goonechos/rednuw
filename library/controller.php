<?php
namespace Library;

/**
 * Main Controller
 */
abstract class Controller
{

    /**
     * Exec before controller action (Why not __construct() ?)
     *
     * @param string $action
     */
    public function init($action = '')
    {}

    /**
     * Exec after controller action (Why not __destruct() ?)
     *
     * @param string $action
     */
    public function endit($action = '')
    {
        // define auto view for an action
        if ($action && ! View::$setted && ! View::$disabled) {
            View::set($action);
        }
    }
}