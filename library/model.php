<?php
namespace Library;

/**
 * Class model manager
 */
class Model extends \SimpleOrm
{

    public function startTransaction()
    {
        self::getConnection()->autocommit(false);
    }

    public function commit()
    {
        self::getConnection()->commit();
    }

    public function rollback()
    {
        self::getConnection()->rollback();
    }
}